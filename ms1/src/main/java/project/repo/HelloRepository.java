package project.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import project.entity.HelloEntity;

public interface HelloRepository extends JpaRepository<HelloEntity,Long> {
}


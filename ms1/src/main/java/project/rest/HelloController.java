package project.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.entity.HelloEntity;
import project.repo.HelloRepository;

@RestController
@RequestMapping("/hello")
@RequiredArgsConstructor
public class HelloController {
    private final HelloRepository helloRepository;
    int count = 0;
    @GetMapping
    public String hello(){
        HelloEntity helloEntity = new HelloEntity();
        String msg = "Hello from ms1 " + ++count;
        helloEntity.setMessage(msg);
        helloEntity.setCount(count);
        helloRepository.save(helloEntity);
        return msg;

    }
}

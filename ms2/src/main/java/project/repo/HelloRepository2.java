package project.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import project.entity.HelloEntity2;

public interface HelloRepository2 extends JpaRepository<HelloEntity2,Long> {
}


package project.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.entity.HelloEntity2;
import project.repo.HelloRepository2;

@RestController
@RequestMapping("/hello2")
@RequiredArgsConstructor
public class HelloController {
    private final HelloRepository2 helloRepository2;
    int count = 0;
    @GetMapping
    public String hello(){
        HelloEntity2 helloEntity2 = new HelloEntity2();
        String msg1 = "Hello from ms2 " + ++count;
        helloEntity2.setMessage(msg1);
        helloEntity2.setCount(count);
        helloRepository2.save(helloEntity2);
        return msg1;

    }
}
